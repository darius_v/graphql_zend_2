<?php

//namespace Application\GraphQL\Type\Factories;

use Application\GraphQL\Type\QueryType;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Model\PersonTable;

class QueryTypeFactory implements FactoryInterface{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new QueryType($container->get(PersonTable::class));
    }



}