<?php

// As this is an empty array, whenever the autoloader looks for a class within the Album namespace, it will fall back to the to StandardAutoloader for us.

// https://framework.zend.com/manual/2.4/en/user-guide/modules.html
return array();
