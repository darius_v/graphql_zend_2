<?php
namespace Application\GraphQL\Type;

/*use GraphQL\Examples\Blog\Data\Story;
use GraphQL\Examples\Blog\Data\User;
use GraphQL\Examples\Blog\Data\Image;
use GraphQL\Examples\Blog\Types;*/
use Application\GraphQL\Types;
use Application\Model\Person;
//use GraphQL\Type\Definition;
use GraphQL\Type\Definition\InterfaceType;


// todo - why node type, what is it?
class NodeType extends InterfaceType
{
    public function __construct()
    {
        $config = [
            'name' => 'Node',
            'fields' => [
                'id' => Types::id()
            ],
            'resolveType' => [$this, 'resolveNodeType']
        ];
        parent::__construct($config);
    }

    public function resolveNodeType($object)
    {
        if ($object instanceof Person) {
            return Types::person();
        } /*else if ($object instanceof Image) {
            return Types::image();
        } else if ($object instanceof Story) {
            return Types::story();
        }*/
    }
}
