<?php


namespace Application\GraphQL\Type;

use Application\GraphQL\Types;
use Application\Model\Person;
use Application\Model\PersonTable;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Zend\Db\TableGateway\TableGateway;



class QueryType extends ObjectType
{
    protected $serviceLocator;

    private $table;

    public function __construct(PersonTable $table)
    {
        $config = [
            'name' => 'Query',
            'fields' => [
                'person' => [
                    'type' => Types::person(),
                    'description' => 'Returns person by id',
                    'args' => [
                        'id' => Types::nonNull(Types::id())
                    ]
                ],
                'hello' => Type::string()
            ],
            'resolveField' => function($val, $args, $context, ResolveInfo $info) {
                return $this->{$info->fieldName}($val, $args, $context, $info);
            }
        ];

        $this->table = $table;
        parent::__construct($config);
    }

    public function person($rootValue, $args)
    {

        /*$person = new Person();

        $person->id = 5;
        $person->firstName = 'Darius';

        return $person;*/

        //https://framework.zend.com/manual/2.3/en/user-guide/database-and-models.html

        // todo - kaip gaut service locator arba di
//        if (!$this->albumTable) {
//            $sm = $this->getServiceLocator();
//            $this->albumTable = $sm->get('Album\Model\AlbumTable');
//        }


        return $this->table->getPerson($args['id']);
    }

    public function hello()
    {
        return 'Your graphql-php endpoint is ready! Use GraphiQL to browse API aaa';
    }
}
