<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller {

use Application\GraphQL\Type\QueryType;
    use Zend\Db\TableGateway\TableGateway;
    use Zend\Mvc\Controller\AbstractActionController;



use Zend\Di\Di;

use Application\GraphQL\Types;
use Application\GraphQL\AppContext;

use \GraphQL\Schema;
use \GraphQL\GraphQL;
use \GraphQL\Type\Definition\Config;
use \GraphQL\Error\FormattedError;

// so that set_error_handler can use and show php errors
use ErrorException;

class IndexController extends AbstractActionController
{
    // by https://github.com/webonyx/graphql-php/tree/master/examples/01-blog
    /* Can run this:
     *  {
            hello
        }
     */

    public $queryType;

    /*public function __construct(QueryType $queryType)
    {
        $this->queryType = $queryType;
    }*/

    public function indexAction()
    {
        if (!empty($_GET['debug'])) {
            // Enable additional validation of type configs
            // (disabled by default because it is costly)
            Config::enableValidation();
            // Catch custom errors (to report them in query results if debugging is enabled)
            $phpErrors = [];
            set_error_handler(function($severity, $message, $file, $line) use (&$phpErrors) {
                $phpErrors[] = new ErrorException($message, 0, $severity, $file, $line);
            });
        }
       // try {
           /* // Initialize our fake data source
            DataSource::init();*/
            // Prepare context that will be available in all field resolvers (as 3rd argument):
            $appContext = new AppContext();

            // here we can change to repository which returns user object
            //$appContext->viewer = DataSource::findUser('1'); // simulated "currently logged-in user"
            $appContext->rootUrl = 'http://localhost:8080';
            $appContext->request = $_REQUEST;
            // Parse incoming query and variables
            if (isset($_SERVER['CONTENT_TYPE']) && strpos($_SERVER['CONTENT_TYPE'], 'application/json') !== false) {
                $raw = file_get_contents('php://input') ?: '';
                $data = json_decode($raw, true);
            } else {
                $data = $_REQUEST;
            }
            $data += ['query' => null, 'variables' => null];
            if (null === $data['query']) {
                $data['query'] = '{hello}';
            }

            /*$di = new Di;

            $di->instanceManager()->setParameters(
            //$di->getInstanceManager()->setProperty(
                'Application\GraphQL\Type\QueryType',
                [$di->get('Zend\Db\TableGateway\TableGateway')]
                );
            $queryType = $di->get('Application\GraphQL\Type\QueryType', new TableGateway('person', $adapter = new Zend\Db\Adapter\Adapter($configArray);));*/


            $personTable = $this->getServiceLocator()->get('Application\Model\PersonTable');

            $queryType = $this->getServiceLocator()->get('Application\GraphQL\Type\QueryType');

//            die('a');
            // GraphQL schema to be passed to query executor:
            $schema = new Schema([
                //'query' => Types::query()
                //'query' => $this->queryType
                'query' => $queryType
            ]);
            $result = GraphQL::execute(
                $schema,
                $data['query'],
                null,
                $appContext,
                (array) $data['variables']
            );
            // Add reported PHP errors to result (if any)
            if (!empty($_GET['debug']) && !empty($phpErrors)) {
                $result['extensions']['phpErrors'] = array_map(
                    ['GraphQL\Error\FormattedError', 'createFromPHPError'],
                    $phpErrors
                );
            }
            $httpStatus = 200;
//        } catch (\Exception $error) {
//            $httpStatus = 500;
//
//            print_r($error);
//
//            if (!empty($_GET['debug'])) {
//                //$result['extensions']['exception'] = FormattedError::createFromException($error);
//            } else {
//                //$result['errors'] = [FormattedError::create('Unexpected Error')];
//            }
//        }
        header('Content-Type: application/json', true, $httpStatus);
        echo json_encode($result);
        die;
    }
}

}

/*namespace {
    // bootstrap
    include 'zf2bootstrap' . ((stream_resolve_include_path('zf2bootstrap.php')) ? '.php' : '.dist.php');
    $di = new Zend\Di\Di;
    $indexController = $di->get('Application\Controller\IndexController');

    // expression to test
    $works = ($indexController->queryType instanceof Application\GraphQL\Type\QueryType);
    // display result
    echo (($works) ? 'It works!' : 'It DOES NOT work!') . PHP_EOL;
}*/