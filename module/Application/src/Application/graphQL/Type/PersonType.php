<?php

namespace Application\GraphQL\Type;

use Application\GraphQL\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;

class PersonType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'Person',
            'description' => 'Persons',
            'fields' => function() {
                return [
                    // todo - why id is return
                    'id' => Types::id(),
                    /*'id' => [
                        'type' => Types::id(),
                    ],*/
                    'firstName' => [
                        'type' => Types::string(),
                    ],
                ];
            },
            // todo what is this
            'interfaces' => [

                Types::node()
            ],
            'resolveField' => function($value, $args, $context, ResolveInfo $info) {

                //if (method_exists($this, $info->fieldName)) {
                    // TODO why this is used? When getting id - it returns value 'ID' instead of number
                    // return $this->{$info->fieldName}($value, $args, $context, $info);
                //} else {
                    // $value is our object found in database, for example Person
                    return $value->{$info->fieldName};
                //}
            }
        ];
        parent::__construct($config);
    }

}
